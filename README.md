# patients_shiny

## 1. Preparing environment

1. Download and Install R from [CRAN](https://cran.r-project.org/)
2. Download and Install [RStudio Desktop](https://www.rstudio.com/products/rstudio/download/#download)
3. Clone the repository with `git clone https://gitlab.com/jan.kucera/patients_shiny` or download zip at https://gitlab.com/jan.kucera/patients_shiny/-/archive/master/patients_shiny-master.zip

## 2. Running Application

1. Open the `patients.Rproj` in RStudio.
2. Install the needed packages and libraries mentioned in `helpers.R`:

    ```R
    install.package('bupaR', 'data.table', 'lubridate', 'shinydashboard', 'plotly', 'DiagrammeR', 'shiny')
    ```

3. Run the application with `shiny::runApp()` or by opening the `server.R` and clicking the 'Run App' green arrow in the top right corner.

## 3. Making the changes

The functions that should be changed are now working (they are returning the data in correct format but incorrect values). There are `TODO:` mentions in the code with description of the task. The changes should be in some ways similar to blocks of code, that are already used elsewhere.

## Documentation

Documentation for the the technologies used can be found all over the internet, few pointers: 

### Shiny

Framework used for running the webapp in the browser.

- sufficeint minimal example [Hello Shiny](https://shiny.rstudio.com/articles/basics.html)
- some description of the key [concepts](https://shiny.rstudio.com/articles/build.html)
- [video tutorial](https://shiny.rstudio.com/tutorial)

### Data.table

For data manipulation and analysis a package `data.table` is used. Probably all of the mentions of `patients_dt` and variables derived from it are of class `data.table`. 

- probably sufficient [`Intro to DT[i, j, by]`](https://www.datacamp.com/community/tutorials/data-table-r-tutorial)
- Short description with [50 examples](https://www.listendata.com/2016/10/r-data-table.html)
- [Cran Vignette Introduction](https://cran.r-project.org/web/packages/data.table/vignettes/datatable-intro.html)
